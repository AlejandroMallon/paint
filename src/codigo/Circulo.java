/*
 * Mi primera clase java

La clase circulo permite almacenar un círculo
además de su posición en la pantalla, si está relleno o no
su color etc


*/
package codigo;

import java.awt.Color;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author alejandro
 */
public class Circulo extends Ellipse2D.Double{
    
    public Color color = null;
    public boolean relleno = false;

}
